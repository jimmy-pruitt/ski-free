const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
require("@babel/register");

// Webpack Configuration
const config = {

    entry: ['babel-polyfill', './src/index.js'],

    output: {
        path: path.resolve(__dirname, './dist'),
        filename: 'bundle.js',
    },

    module: {
        rules : [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: ['babel-loader'],
            },
            {
                test: /\.css$/,
                use: ['style-loader', 'css-loader'],
            }
        ]
    },

    plugins: [
        new htmlWebpackPlugin({
            title: 'Ceros Ski'
        }),
        new CopyPlugin([
            { from: 'img', to: 'img' }
        ])
    ],

    devServer: {
        host: '0.0.0.0', // I was using Docker because I didn't want to install Node on my personal computer
        watchOptions: {  // and I didn't want to have to keep removing this setup here before I commit
            aggregateTimeout: 500,
            poll: 1000
        }
    },
};

module.exports = config;