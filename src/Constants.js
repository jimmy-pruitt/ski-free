export const GAME_WIDTH = window.innerWidth;
export const GAME_HEIGHT = window.innerHeight;

export const SKIER_STATIONARY = 'skierStationary';
export const SKIER_LEFT = 'skierLeft';
export const SKIER_LEFTDOWN = 'skierLeftDown';
export const SKIER_DOWN = 'skierDown';
export const SKIER_RIGHTDOWN = 'skierRightDown';
export const SKIER_RIGHT = 'skierRight';
export const SKIER_JUMP_START = 'skierJump1';
export const SKIER_JUMP_TUCK = 'skierJump2';
export const SKIER_JUMP_ROLL = 'skierJump3';
export const SKIER_JUMP_LAND = 'skierJump4';

export const RHINO_DEFAULT = 'rhinoDefault';
export const RHINO_RUN_1 = 'rhinoRun1';
export const RHINO_RUN_2 = 'rhinoRun2';
export const RHINO_EAT_1 = 'rhinoEat1';
export const RHINO_EAT_2 = 'rhinoEat2';
export const RHINO_EAT_3 = 'rhinoEat3';
export const RHINO_EAT_4 = 'rhinoEat4';
export const RHINO_EAT_5 = 'rhinoEat5';
export const RHINO_EAT_6 = 'rhinoEat6';


export const TREE = 'tree';
export const TREE_CLUSTER = 'treeCluster';
export const ROCK1 = 'rock1';
export const ROCK2 = 'rock2';
export const RAMP = 'ramp';

export const ASSETS = {
    [SKIER_STATIONARY]: 'img/skier_crash.png',
    [SKIER_LEFT]: 'img/skier_left.png',
    [SKIER_LEFTDOWN]: 'img/skier_left_down.png',
    [SKIER_DOWN]: 'img/skier_down.png',
    [SKIER_RIGHTDOWN]: 'img/skier_right_down.png',
    [SKIER_RIGHT]: 'img/skier_right.png',
    [SKIER_JUMP_START] : 'img/skier_jump_1.png',
    [SKIER_JUMP_TUCK] : 'img/skier_jump_3.png',
    [SKIER_JUMP_ROLL] : 'img/skier_jump_2.png',
    [SKIER_JUMP_LAND] : 'img/skier_jump_1.png',

    [RHINO_DEFAULT] : 'img/rhino_default.png',
    [RHINO_RUN_1] : 'img/rhino_run_left.png',
    [RHINO_RUN_2] : 'img/rhino_run_left_2.png',
    [RHINO_EAT_1] : 'img/rhino_lift.png',
    [RHINO_EAT_2] : 'img/rhino_lift_mouth_open.png',
    [RHINO_EAT_3] : 'img/rhino_lift_eat_1.png',
    [RHINO_EAT_4] : 'img/rhino_lift_eat_2.png',
    [RHINO_EAT_5] : 'img/rhino_lift_eat_3.png',
    [RHINO_EAT_6] : 'img/rhino_lift_eat_4.png',

    [TREE] : 'img/tree_1.png',
    [TREE_CLUSTER] : 'img/tree_cluster.png',
    [ROCK1] : 'img/rock_1.png',
    [ROCK2] : 'img/rock_2.png',
    [RAMP] : 'img/jump_ramp.png',
};

export const SKIER_TRAJECTORIES = {
    STATIONARY : 0,
    LEFT : 1,
    LEFT_DOWN : 2,
    DOWN : 3,
    RIGHT_DOWN : 4,
    RIGHT : 5,
};

export const SKIER_DIRECTION_ASSET = {
    [SKIER_TRAJECTORIES.STATIONARY] : SKIER_STATIONARY,
    [SKIER_TRAJECTORIES.LEFT] : SKIER_LEFT,
    [SKIER_TRAJECTORIES.LEFT_DOWN] : SKIER_LEFTDOWN,
    [SKIER_TRAJECTORIES.DOWN] : SKIER_DOWN,
    [SKIER_TRAJECTORIES.RIGHT_DOWN] : SKIER_RIGHTDOWN,
    [SKIER_TRAJECTORIES.RIGHT] : SKIER_RIGHT,
};

export const SKIER_STATES = {
    CRASHED: 0,
    DEAD: 1,
    SKIING: 2,
    JUMP_START : 3,
    JUMP_TUCK : 4,
    JUMP_ROLL : 5,
    JUMP_LAND : 6,
};

export const SKIER_JUMPING_ASSET = {
    [SKIER_STATES.JUMP_START] : SKIER_JUMP_START,
    [SKIER_STATES.JUMP_TUCK] : SKIER_JUMP_TUCK,
    [SKIER_STATES.JUMP_ROLL] : SKIER_JUMP_ROLL,
    [SKIER_STATES.JUMP_LAND] : SKIER_JUMP_LAND,
};

export const RHINO_STATES = {
    SLEEPING: 0,
    CHASING: 1,
    EATING_1: 2,
    EATING_2: 3,
    EATING_3: 4,
    EATING_4: 5,
    EATING_5: 6,
    EATING_6: 7,
    CELEBRATING: 8,
};

export const KEYS = {
    SPACE : 32,
    LEFT : 37,
    RIGHT : 39,
    UP : 38,
    DOWN : 40
};