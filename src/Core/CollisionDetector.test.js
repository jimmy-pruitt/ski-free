import * as Constants from '../Constants';
import { ObstacleManager } from '../Entities/Obstacles/ObstacleManager';
import { Skier } from '../Entities/Skier';
import { Obstacle } from '../Entities/Obstacles/Obstacle';
import { CollisionDetector } from './CollisionDetector';
import { Rhino } from '../Entities/Rhino';

describe('CollisionDetector', () => {
    const jumpStates = [
        Constants.SKIER_STATES.JUMP_START,
        Constants.SKIER_STATES.JUMP_TUCK,
        Constants.SKIER_STATES.JUMP_ROLL,
        Constants.SKIER_STATES.JUMP_LAND
    ];

    const mockedAssetManager = new class {
        getAsset(assetName) {
            if (!assetName) {
                return undefined;
            }

            if (assetName.startsWith('skier')) {
                return {
                    width: 2,
                    height: 4
                };
            }

            return {
                width: 10,
                height: 12
            }
        }
    };

    let obstacleManager;
    beforeEach(() => {
        obstacleManager = new ObstacleManager();
    });

    describe('skierDidCrash', () => {
        describe('when the skier occupies the same space as an obstacle', () => {
            let skier;
            let obstacleInQuestion;
            beforeEach(() => {
                skier = new Skier(500, 600);
                obstacleInQuestion = new Obstacle(500, 600);
                obstacleManager.obstacles = [obstacleInQuestion, new Obstacle(1000, 2000)];
            });

            describe('and that obstacle is not a rock or ramp', () => {
                const totallyLethalAssets = [ Constants.TREE, Constants.TREE_CLUSTER ];

                it('should return true', () => {
                    const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                    for (const asset of totallyLethalAssets) {
                        obstacleInQuestion.assetName = asset;
                        expect(collisionDetector.skierDidCrash()).toBe(true);
                    }
                });
            });

            describe('and that obstacle is a rock', () => {
                const rockAssets = [ Constants.ROCK1, Constants.ROCK2 ];

                describe('and the skier is not currently jumping', () => {
                    it('should return true', () => {
                        const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                        for (const rockAsset of rockAssets) {
                            obstacleInQuestion.assetName = rockAsset;
                            expect(collisionDetector.skierDidCrash()).toBe(true);
                        }
                    });
                });

                describe('and the skier is currently jumping', () => {
                    it('should return fale', () => {
                        const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                        for (const jumpState of jumpStates) {
                            skier.setState(jumpState);

                            for (const rockAsset of rockAssets) {
                                obstacleInQuestion.assetName = rockAsset;
                                expect(collisionDetector.skierDidCrash()).toBe(false);
                            }
                        }
                    });
                });
            });

            describe('and that obstacle is a ramp', () => {
                beforeEach(() => {
                    obstacleInQuestion.assetName = Constants.RAMP;
                });

                describe("and the skier hasn't jumped recently (and is not currently jumping)", () => {
                    it('should return false', () => {
                        const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                        expect(collisionDetector.skierDidCrash()).toBe(false);
                    });
                });

                describe('and the skier is currently jumping', () => {
                    // This is probably a controversial decision but I chose for the skier not to be
                    // allowed to jump over ramps. While the asset is only two dimensional I imagine that
                    // the third dimension that we can't see, the height of the ramp, is 300 feet
                    // and our skier is the one and only Shaun White. The dude is a badass but ain't
                    // nobody gonna jump OVER a 300 foot tall ramp in a single bound
                    it('should return true', () => {
                        const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                        for (const jumpState of jumpStates) {
                            skier.setState(jumpState);
                            expect(collisionDetector.skierDidCrash()).toBe(true);
                        }
                    });
                });

                describe('and the skier has jumped recently', () => {
                    beforeEach(() => {
                        skier.jump();
                        skier.move();
                        skier.move();
                        obstacleInQuestion.x = skier.x;
                        obstacleInQuestion.y = skier.y;
                    });

                    // I've chosen to let the cooldown timer apply to ramps as well.
                    // Jumping once takes all the wind out of the little guy
                    it('should return true', () => {
                        // Just to verify that the test is still set up correctly
                        expect(skier.canJump()).toBe(false);

                        const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                        expect(collisionDetector.skierDidCrash()).toBe(true);
                    });
                });
            });
        });

        describe("when the skier doesn't occupy the same space as an obstacle", () => {
            let skier;
            let obstacleInQuestion;
            beforeEach(() => {
                skier = new Skier(200, 800);
                obstacleInQuestion = new Obstacle(300, 100);
                obstacleManager.obstacles = [obstacleInQuestion, new Obstacle(1000, 2000)];
            });

            it("should return false", () => {
                const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                expect(collisionDetector.skierDidCrash()).toBe(false);
            });
        });
    });

    describe('skierDidJumpOffRamp', () => {
        let skier;

        beforeEach(() => {
            skier = new Skier(100, 200);
        });

        describe('when the skier and a ramp occupy the same space', () => {
            let obstacleInQuestion;
            beforeEach(() => {
                obstacleInQuestion = new Obstacle(100, 200);
                obstacleInQuestion.assetName = Constants.RAMP;
                obstacleManager.obstacles = [ new Obstacle(10, 20), obstacleInQuestion ];
            });

            describe('and the skier is able to jump', () => {
                beforeEach(() => {
                    skier.canJump = () => true;
                });

                it('should return true', () => {
                    const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                    expect(collisionDetector.skierDidJumpOffRamp()).toBe(true);
                });
            });

            describe('and the skier is not able to jump', () => {
                beforeEach(() => {
                    skier.canJump = () => false;
                });

                it('should return false', () => {
                    const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                    expect(collisionDetector.skierDidJumpOffRamp()).toBe(false);
                });
            });
        });

        describe('when no ramps occupy the same space as the skier', () => {
            beforeEach(() => {
                const ramp1 = new Obstacle(10, 20);
                ramp1.assetName = Constants.RAMP;
                const ramp2 = new Obstacle(20, 40);
                ramp2.assetName = Constants.RAMP;
                obstacleManager.obstacles = [ ramp1, new Obstacle(10, 20), ramp2 ];
            });

            // These are mostly just defensive cases, in the event that logic
            // in the class accidentally makes the function return true when it shouldn't
            describe('and the skier is able to jump', () => {
                beforeEach(() => {
                    skier.canJump = () => true;
                });

                it('should return false', () => {
                    const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                    expect(collisionDetector.skierDidJumpOffRamp()).toBe(false);
                });
            });

            describe('and the skier is not able to jump', () => {
                beforeEach(() => {
                    skier.canJump = () => false;
                });

                it('should return false', () => {
                    const collisionDetector = new CollisionDetector(skier, new Rhino(), obstacleManager, mockedAssetManager);
                    expect(collisionDetector.skierDidJumpOffRamp()).toBe(false);
                });
            });
        });
    });
});