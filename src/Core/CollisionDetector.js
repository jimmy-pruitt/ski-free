import * as Constants from '../Constants';
import { intersectTwoRects, Rect } from "../Core/Utils";

export class CollisionDetector {
    skier;
    rhino;
    obstacleManager;
    assetManager;

    constructor(skier, rhino, obstacleManager, assetManager) {
        this.skier = skier;
        this.rhino = rhino;
        this.obstacleManager = obstacleManager;
        this.assetManager = assetManager;
    }

    skierDidCrash() {
        const didHitLethalObject = this.obstacleManager.getLethalObstacles().some(this.verifyCollision.bind(this));
        return didHitLethalObject || (!this.skier.canJump() && this.skierIsOnRamp());
    }

    skierDidJumpOffRamp() {
        return this.skier.canJump() && this.skierIsOnRamp();
    }

    skierIsOnRamp() {
        return this.obstacleManager.getRamps().some(this.verifyCollision.bind(this));
    }

    getSkierBounds() {
        const asset = this.assetManager.getAsset(this.skier.assetName);
        if (!asset) {
            return undefined;
        }

        return new Rect(
            this.skier.x - asset.width / 2,
            this.skier.y - asset.height / 2,
            this.skier.x + asset.width / 2,
            this.skier.y - asset.height / 4
        );
    }

    verifyCollision(entity) {
        const skierBounds = this.getSkierBounds();
        if (!skierBounds) {
            return false;
        }

        const entityAsset = this.assetManager.getAsset(entity.getAssetName());
        const entityPosition = entity.getPosition();
        const entityBounds = new Rect(
            entityPosition.x - entityAsset.width / 2,
            entityPosition.y - entityAsset.height / 2,
            entityPosition.x + entityAsset.width / 2,
            entityPosition.y
        );

        const wouldCollide = intersectTwoRects(skierBounds, entityBounds);
        if (!this.skier.isJumping()) {
            return wouldCollide;
        }

        return wouldCollide && !entity.assetName.startsWith('rock');
    }
}