import * as Constants from '../Constants';
import { Rhino } from './Rhino';
import { Entity } from './Entity';
import { Skier } from './Skier';

beforeAll(() => {
    Constants.GAME_WIDTH = 1200;
    Constants.GAME_HEIGHT = 600;
});

afterAll(() => {
    delete require.cache[require.resolve('../Constants.js')]; // To restore constants, for other test suites
});

describe('Rhino', () => {
    describe('constructor', () => {
        it('should set the x, y, and lunch properties on the object', () => {
            const targetToChase = new Entity(80, 100);
            const rhino = new Rhino(10, 20, targetToChase);
            expect(rhino.x).toBe(10);
            expect(rhino.y).toBe(20);
            expect(rhino.lunch).toBe(targetToChase);
        });
    });

    describe('updateAsset', () => {
        let rhino;
        beforeEach(() => {
            rhino = new Rhino();
        });

        describe('when the rhino is sleeping', () => {
            beforeEach(() => {
                rhino.setState(Constants.RHINO_STATES.SLEEPING);
            })

            it('should set an undefined asset', () => {
                rhino.updateAsset();
                expect(rhino.asssetName).toBe(undefined);
            })
        })

        // This here is the source for a silly looking run animation on the Rhino.
        // Every frame will switch the asssets, instead of transitioning more smoothly.
        // With more time I probably would have smoothed it out, but didn't think it
        // would be a big deal
        describe('when the rhino is still chasing the skier', () => {
            beforeEach(() => {
                rhino.setState(Constants.RHINO_STATES.CHASING);
            })

            describe("and the rhino's left foot is on the ground", () => {
                beforeEach(() => {
                    rhino.assetName = Constants.RHINO_RUN_1;
                });

                it('should replace it with his right foot', () => {
                    rhino.updateAsset();
                    expect(rhino.assetName).toBe(Constants.RHINO_RUN_2);
                })
            });

            describe("and the rhino's right foot is on the ground", () => {
                beforeEach(() => {
                    rhino.assetName = Constants.RHINO_RUN_2;
                });

                it('should replace it with his left foot', () => {
                    rhino.updateAsset();
                    expect(rhino.assetName).toBe(Constants.RHINO_RUN_1);
                })
            });
        })

        describe('when the rhino is in the middle of a feeding frenzy', () => {
            const eatingStates = [
                Constants.RHINO_STATES.EATING_1,
                Constants.RHINO_STATES.EATING_2,
                Constants.RHINO_STATES.EATING_3,
                Constants.RHINO_STATES.EATING_4,
                Constants.RHINO_STATES.EATING_5,
                Constants.RHINO_STATES.EATING_6,
            ];

            it('should set the correct asset', () => {
                for (let i = 0; i < eatingStates; ++i) {
                    rhino.setState(eatingStates[i]);
                    rhino.updateAsset();
                    expect(rhino.asssetName).toBe(`rhinoEat${i + 1}`);
                }
            })
        })

        describe('when the rhino is in any state besides eating, sleeping, or chasing', () => {
            beforeEach(() => {
                rhino.setState(Constants.RHINO_STATES.CELEBRATING);
                rhino.assetName = Constants.RHINO_DEFAULT;
            });

            it('should not change the asset', () => {
                rhino.updateAsset();
                expect(rhino.assetName).toBe(Constants.RHINO_DEFAULT);
            })
        });
    });

    describe('hunt', () => {
        let rhino;
        let skier;
        beforeEach(() => {
            skier = new Skier();
            rhino = new Rhino(undefined, undefined, skier);
        });

        describe('when the rhino is still sleeping', () => {
            beforeEach(() => {
                rhino.setState(Constants.RHINO_STATES.SLEEPING);
            });

            describe("and the rhino doesn't wake up this turn", () => {
                beforeEach(() => {
                    rhino.framesSpentSleeping = Rhino.FRAMES_BEFORE_APPEARING - 2;
                })

                it('should increment the sleep timer, but not wake up the rhino', () => {
                    rhino.hunt();
                    expect(rhino.state).toBe(Constants.RHINO_STATES.SLEEPING);
                    expect(rhino.framesSpentSleeping).toBe(Rhino.FRAMES_BEFORE_APPEARING -1);
                })
            })

            describe('and the rhino wakes up this turn', () => {
                beforeEach(() => {
                    rhino.framesSpentSleeping = Rhino.FRAMES_BEFORE_APPEARING - 1;
                    skier.x = 800;
                    skier.y = 300;
                })

                it('should start chasing the skier', () => {
                    rhino.hunt();
                    expect(rhino.state).toBe(Constants.RHINO_STATES.CHASING);

                    expect(rhino.y).toBe(150);
                    expect(Math.abs(rhino.x - skier.x)).toBe(300);
                })
            })
        });
    });

    describe('interpolateNextPosition', () => {
        let skier;
        let rhino;
        beforeEach(() => {
            skier = new Skier(1000, 500);
            rhino = new Rhino(undefined, undefined, skier);
        })

        describe('when the rhino is straight above the skier', () => {
            beforeEach(() => {
                rhino.x = 1000;
                rhino.y = 225
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(rhino.x).toBe(1000);
                expect(Math.round(rhino.y)).toBe(235);
            });
        });

        describe('when the rhino is above the skier and to the left', () => {
            beforeEach(() => {
                rhino.x = 870;
                rhino.y = 225
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(874);
                expect(Math.round(rhino.y)).toBe(234);
            });
        });

        describe('when the rhino is above the skier and to the right', () => {
            beforeEach(() => {
                rhino.x = 1100;
                rhino.y = 225
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(1097);
                expect(Math.round(rhino.y)).toBe(234);
            });
        });

        describe('when the rhino is straight left of the skier', () => {
            beforeEach(() => {
                rhino.x = 225;
                rhino.y = 500
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(235);
                expect(rhino.y).toBe(500);
            });
        });

        describe('when the rhino is straight right of the skier', () => {
            beforeEach(() => {
                rhino.x = 1200;
                rhino.y = 500;
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(1190);
                expect(rhino.y).toBe(500);
            });
        });

        // I'm not sure this case is technically possible in game, but just in case
        describe('when the rhino is straight below the skier', () => {
            beforeEach(() => {
                rhino.x = 1000;
                rhino.y = 600
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(rhino.x).toBe(1000);
                expect(Math.round(rhino.y)).toBe(590);
            });
        });

        describe('when the rhino is below the skier and to the left', () => {
            beforeEach(() => {
                rhino.x = 700;
                rhino.y = 600
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(710);
                expect(Math.round(rhino.y)).toBe(597);
            });
        });

        describe('when the rhino is below the skier and to the right', () => {
            beforeEach(() => {
                rhino.x = 1900;
                rhino.y = 600
            });

            it('should move the rhino closer to the skier', () => {
                rhino.interpolateNextPosition();
                expect(Math.round(rhino.x)).toBe(1890);
                expect(Math.round(rhino.y)).toBe(599);
            });
        });
    });
});