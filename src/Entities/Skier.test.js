import "babel-polyfill";
import { Skier } from "./Skier";
import * as Constants from '../Constants';
import { AssetManager } from "../Core/AssetManager";
import { ObstacleManager } from "./Obstacles/ObstacleManager";
import { Obstacle } from "./Obstacles/Obstacle";

describe('Skier', () => {
    describe('contructor', () => {
        it('should set the X and Y properties on the skier', () => {
            const x = 50;
            const y = 200;
            const skier = new Skier(x, y);
            expect(skier.x).toBe(x);
            expect(skier.y).toBe(y);
        });
    });

    describe('setDirection', () => {
        it('should set the direction property on the skier and update its asset', () => {
            const skier = new Skier();

            // This would just be a check for other developers. If they changed the default direction of the
            // skier then it would potentially change the behaviour of this describe
            expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);

            skier.setDirection(Constants.SKIER_TRAJECTORIES.STATIONARY);
            expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.STATIONARY);
            expect(skier.assetName).toBe(Constants.SKIER_STATIONARY);
        });
    });

    describe('move', () => {
        it("should decrement the skier's jump cooldown, but no lower than 0", () => {
            let skier = new Skier(0, 0);
            skier.jumpCooldownTimer = 1;
            skier.move();
            expect(skier.jumpCooldownTimer).toBe(0);
            skier.move();
            expect(skier.jumpCooldownTimer).toBe(0);
        });

        describe('when the skier is facing straight left', () => {
            // I am a very devout acolyte of the DRY principle in production code but I personally
            // don't adhere to it so strictly in tests. A lot of my tests will individually redeclare
            // and reinstantiate the object under test when it really could be moved up to a global beforeEach.
            // I prefer it the way I've written it personally because
            // A) changes to one test case have a lower risk of inadvertently changing another case
            // and
            // B) the important details of the test are all on the screen at once. There's less need to
            // constantly scroll to the top and back down to verify conditions and it's especially useful
            // when a test has failed and you're trying to determine why. The further up in the chain you go,
            // the harder it becomes to locate the source of a failure, in my opinion
            let skier;

            beforeEach(() => {
                skier = new Skier(500, 600);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT);
            });

            it('should move the skier left 10 units', () => {
                skier.move();
                expect(skier.x).toBe(500 - Skier.STARTING_SPEED);
                expect(skier.y).toBe(600);
            })
        });

        describe('when the skier is facing diagonally left', () => {
            let skier;

            beforeEach(() => {
                skier = new Skier(200, 300);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT_DOWN);
            });

            it('should move the skier left and down simultaneously', () => {
                skier.move();
                const expectedShift = (Skier.STARTING_SPEED / Skier.DIAGONAL_SPEED_REDUCER);
                expect(skier.x).toBe(200 - expectedShift);
                expect(skier.y).toBe(300 + expectedShift);
            })
        });

        describe('when the skier is facing straight down', () => {
            let skier;

            beforeEach(() => {
                skier = new Skier(800, 700);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
            });

            it('should move the skier straight', () => {
                skier.move();
                expect(skier.x).toBe(800);
                expect(skier.y).toBe(700 + Skier.STARTING_SPEED);
            })
        });

        describe('when the skier is facing diagonally right', () => {
            let skier;

            beforeEach(() => {
                skier = new Skier(125, 250);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT_DOWN);
            });

            it('should move the skier right and down simultaneously', () => {
                skier.move();
                const expectedShift = Skier.STARTING_SPEED / Skier.DIAGONAL_SPEED_REDUCER;
                expect(skier.x).toBe(125 + expectedShift);
                expect(skier.y).toBe(250 + expectedShift);
            });
        });

        describe('when the skier is facing straight right', () => {
            let skier;

            beforeEach(() => {
                skier = new Skier(1000, 2000);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
            });

            it('should move the skier right 10 units', () => {
                skier.move();
                expect(skier.x).toBe(1000 + Skier.STARTING_SPEED);
                expect(skier.y).toBe(2000);
            })
        });

        describe('when the skier is in mid-air', () => {
            const jumpStates = [
                Constants.SKIER_STATES.JUMP_START,
                Constants.SKIER_STATES.JUMP_TUCK,
                Constants.SKIER_STATES.JUMP_ROLL,
                Constants.SKIER_STATES.JUMP_LAND
            ];

            describe("and the skier's been in the same state for exactly 10 frames", () => {
                it('should move the skier down and into the next jump state', () => {
                    for (let i = 0; i < jumpStates; ++i) {
                        const skier = new Skier(20, 40);
                        skier.framesInAir = (10 * i) + 9;
                        skier.setDirection(jumpStates[i]);
                        skier.move();
                        expect(skier.state).toBe(jumpStates[i + 1] || Constants.SKIER_STATES.SKIING);
                        expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                        expect(skier.y).toBe(40 + Skier.STARTING_SPEED);
                    }
                });
            });

            describe("and the skier's been in the same state for less than 10 frames", () => {
                it('should move the skier down but not change state', () => {
                    for (const jumpState of jumpStates) {
                        const skier = new Skier(20, 40);
                        skier.framesInAir = 4;
                        skier.setState(jumpState);
                        skier.move();
                        expect(skier.state).toBe(jumpState);
                        expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                        expect(skier.y).toBe(40 + Skier.STARTING_SPEED);
                    }
                });
            });
        });

        describe('when the skier just landed a jump', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(50, 100);
                skier.setState(Constants.SKIER_STATES.JUMP_LAND);
                skier.framesInAir = 39;
            });

            it('should start the jump cooldown timer', () => {
                skier.move();
                expect(skier.jumpCooldownTimer).toBe(50);
            });

            it('should move the position of the skier down', () => {
                skier.move();
                expect(skier.y).toBe(100 + Skier.STARTING_SPEED);
            });

            it('should put the skier into a downward direction', () => {
                skier.move();
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
            });

            it('should reset the counter for in-air frames', () => {
                skier.move();
                expect(skier.framesInAir).toBe(0);
            });
        });

        describe('when the skier is in any other state', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(10, 20);
                skier.setState(Constants.SKIER_STATES.DEAD);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT_DOWN)
            });

            it('should not move the skier', () => {
                skier.move();
                expect(skier.x).toBe(10);
                expect(skier.y).toBe(20);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.LEFT_DOWN);
            });
        });
    });

    describe('turnLeft', () => {
        describe('while the skier is facing left', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(50, 100);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT);
            });

            it('should move the skier left', () => {
                skier.turnLeft();
                expect(skier.x).toBeLessThan(50);
                expect(skier.y).toBe(100);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.LEFT);
            })
        });

        describe("while the skier isn't facing left", () => {
            describe("and the skier isn't jumping", () => {
                describe("and the skier hasn't crashed", () => {
                    let skier;
                    beforeEach(() => {
                        skier = new Skier(50, 100);
                        skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
                        skier.setState(Constants.SKIER_STATES.SKIING);
                    });

                    it('should rotate the skier clockwise without moving position', () => {
                        skier.turnLeft();
                        expect(skier.x).toBe(50);
                        expect(skier.y).toBe(100);
                        expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.RIGHT_DOWN);
                    });
                });
            });

            describe('and the skier is jumping', () => {
                let skier;
                beforeEach(() => {
                    skier = new Skier(50, 100);
                    skier.setState(Constants.SKIER_STATES.JUMP_START);
                    skier.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
                });

                it('should not change the state or position of the skier', () => {
                    skier.turnLeft();
                    expect(skier.x).toBe(50);
                    expect(skier.y).toBe(100);
                    expect(skier.state).toBe(Constants.SKIER_STATES.JUMP_START);
                    expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                });
            });
        });

        describe('when the skier has crashed', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(50, 100);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.STATIONARY);
                skier.setState(Constants.SKIER_STATES.CRASHED);
            });

            it('should face the skier to the left and resume skiing', () => {
                skier.turnLeft();
                expect(skier.x).toBe(50);
                expect(skier.y).toBe(100);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.LEFT);
                expect(skier.state).toBe(Constants.SKIER_STATES.SKIING);
            })
        });
    });

    describe('turnRight', () => {
        describe('while the skier is facing right', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(50, 100);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
            });

            it('should move the skier right', () => {
                skier.turnRight();
                expect(skier.x).toBeGreaterThan(50);
                expect(skier.y).toBe(100);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.RIGHT);
            });
        });

        describe("while the skier isn't facing right and hasn't crashed", () => {
            describe("and the skier isn't jumping", () => {
                describe("and the skier hasn't crashed", () => {
                    let skier;
                    beforeEach(() => {
                        skier = new Skier(50, 100);
                        skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT);
                        skier.setState(Constants.SKIER_STATES.SKIING);
                    });

                    it('should rotate the skier anti-clockwise without moving position', () => {
                        skier.turnRight();
                        expect(skier.x).toBe(50);
                        expect(skier.y).toBe(100);
                        expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.LEFT_DOWN);
                    });
                });
            });

            describe('and the skier is jumping', () => {
                let skier;
                beforeEach(() => {
                    skier = new Skier(50, 100);
                    skier.setState(Constants.SKIER_STATES.JUMP_START);
                    skier.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
                });

                it('should not change the state or position of the skier', () => {
                    skier.turnRight();
                    expect(skier.x).toBe(50);
                    expect(skier.y).toBe(100);
                    expect(skier.state).toBe(Constants.SKIER_STATES.JUMP_START);
                    expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                });
            });
        });

        describe('when the skier has crashed', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(50, 100);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.STATIONARY);
                skier.setState(Constants.SKIER_STATES.CRASHED);
            });

            it('should face the skier to the right and resume skiing', () => {
                skier.turnRight();
                expect(skier.x).toBe(50);
                expect(skier.y).toBe(100);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.RIGHT);
                expect(skier.state).toBe(Constants.SKIER_STATES.SKIING);
            });
        });
    });

    describe('turnUp', () => {
        describe('when the skier is facing straight left', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(100, 200);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.LEFT);
            });

            it('should move the skier higher on the screen', () => {
                skier.turnUp();
                expect(skier.x).toBe(100);
                expect(skier.y).toBeLessThan(200);

                // This sort of feels like a bug to me. If anything either:
                // A) The skier shouldn't be allowed to move up from this state
                // or
                // B) The skier should continue rotating, but we don't have an asset for that
                // It was like that when I found it though
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.LEFT);
            })
        });

        describe('when the skier is facing straight right', () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(100, 200);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
            });

            it('should move the skier higher on the screen', () => {
                skier.turnUp();
                expect(skier.y).toBeLessThan(200);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.RIGHT);

                // This sort of feels like a bug to me. If anything either:
                // A) The skier shouldn't be allowed to move up from this state
                // or
                // B) The skier should continue rotating, but we don't have an asset for that
                // It was like that when I found it though
                expect(skier.x).toBe(100);
            });
        });

        describe("when the skier isn't facing straight left or straight right", () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(100, 200);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
            });

            it("shouldn't move the skier or change its direction", () => {
                skier.turnUp();
                expect(skier.x).toBe(100);
                expect(skier.y).toBe(200);
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
            });
        });
    });

    describe('turnDown', () => {
        describe("when the skier isn't in the middle of a jump", () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(20, 40);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
                skier.setState(Constants.SKIER_STATES.SKIING);
            });

            it("should override the skier's current direction", () => {
                skier.turnDown();
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                expect(skier.x).toBe(20);
                expect(skier.y).toBe(40);
            });
        });

        describe("when the skier is in the middle of a jump", () => {
            let skier;
            beforeEach(() => {
                skier = new Skier(20, 40);
                skier.setState(Constants.SKIER_STATES.JUMP_TUCK);
                skier.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
            });

            it("should not change the skier's current state or direction", () => {
                skier.turnDown();
                expect(skier.direction).toBe(Constants.SKIER_TRAJECTORIES.DOWN);
                expect(skier.state).toBe(Constants.SKIER_STATES.JUMP_TUCK);
                expect(skier.x).toBe(20);
                expect(skier.y).toBe(40);
            });
        });
    });
});