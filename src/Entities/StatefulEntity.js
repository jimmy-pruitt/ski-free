import { Entity } from './Entity';

export class StatefulEntity extends Entity {
    state;
    direction;

    setState(newState) {
        this.state = newState;
        this.updateAsset(); // If we were in Typescript I would declare
                            // updateAsset as abstract on this class
    }
}