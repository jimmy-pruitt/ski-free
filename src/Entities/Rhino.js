import * as Constants from '../Constants';
import { randomInt } from '..//Core/Utils';
import { StatefulEntity } from './StatefulEntity';

export class Rhino extends StatefulEntity {
    static STARTING_SPEED = 10.05;
    static FRAMES_BEFORE_APPEARING = 600;
    static FRAMES_PER_FINAL_PHASES = 25;

    assetName = Constants.RHINO_DEFAULT;

    state = Constants.RHINO_STATES.SLEEPING;

    speed = Rhino.STARTING_SPEED;

    framesSpentSleeping = 0;
    framesSpentEating = 0;
    framesSpentCelebrating = 0;

    lunch;

    constructor(x, y, lunch) {
        super(x, y);

        this.lunch = lunch;
    }

    updateAsset() {
        switch (this.state) {
            case Constants.RHINO_STATES.SLEEPING:
                this.assetName = undefined;
                break;
            case Constants.RHINO_STATES.CHASING:
                this.assetName = this.assetName === Constants.RHINO_RUN_1 ? Constants.RHINO_RUN_2 : Constants.RHINO_RUN_1;
                break;
            case Constants.RHINO_STATES.EATING_1:
                this.assetName = Constants.RHINO_EAT_1;
                break;
            case Constants.RHINO_STATES.EATING_2:
                this.assetName = Constants.RHINO_EAT_2;
                break;
            case Constants.RHINO_STATES.EATING_3:
                this.assetName = Constants.RHINO_EAT_3;
                break;
            case Constants.RHINO_STATES.EATING_4:
                this.assetName = Constants.RHINO_EAT_4;
                break;
            case Constants.RHINO_STATES.EATING_5:
                this.assetName = Constants.RHINO_EAT_5;
                break;
            case Constants.RHINO_STATES.EATING_6:
                this.assetName = Constants.RHINO_EAT_6;
                break;
        }
    }

    hunt() {
        switch (this.state) {
            case Constants.RHINO_STATES.SLEEPING:
                if (++this.framesSpentSleeping === Rhino.FRAMES_BEFORE_APPEARING) {
                    this.beginChase();
                }
                break;
            case Constants.RHINO_STATES.CHASING:
                this.continueChasing();
                break;
            case Constants.RHINO_STATES.EATING_1:
            case Constants.RHINO_STATES.EATING_2:
            case Constants.RHINO_STATES.EATING_3:
            case Constants.RHINO_STATES.EATING_4:
            case Constants.RHINO_STATES.EATING_5:
            case Constants.RHINO_STATES.EATING_6:
                this.eat();
                break;
            case Constants.RHINO_STATES.CELEBRATING:
                this.celebrate();
        }
    }

    beginChase() {
        this.setState(Constants.RHINO_STATES.CHASING);

        let xOffset = Constants.GAME_WIDTH / 4;
        if (randomInt(0, 1)) {
            xOffset *= -1;
        }

        const startingX = this.lunch.x - xOffset; 
        const startingY = this.lunch.y - Constants.GAME_HEIGHT / 4;
        this.x = startingX;
        this.y = startingY;
    }

    continueChasing() {
        this.updateAsset();
        this.interpolateNextPosition();
    }

    interpolateNextPosition() {
        const distanceFromSkier = Math.sqrt(Math.pow(this.x - this.lunch.x, 2) + Math.pow(this.y - this.lunch.y, 2));
        const singleXUnit = 1 / distanceFromSkier * (this.lunch.x - this.x);
        const singleYUnit = 1 / distanceFromSkier * (this.lunch.y - this.y);
        this.x += singleXUnit * this.speed;
        this.y += singleYUnit * this.speed;
    }

    isHungry() {
        return this.state > Constants.RHINO_STATES.SLEEPING;
    }

    eat() {
        const shouldChangeState = ++this.framesSpentEating % Rhino.FRAMES_PER_FINAL_PHASES === 0;
        if (shouldChangeState) {
            if (this.state === Constants.RHINO_STATES.EATING_6) {
                this.setState(Constants.RHINO_STATES.CELEBRATING);
            } else {
                this.setState(this.state + 1);
            }
        }
    }

    feast() {
        this.setState(Constants.RHINO_STATES.EATING_1);
    }

    celebrate() {
        const shouldChangeAsset = ++this.framesSpentCelebrating % Rhino.FRAMES_PER_FINAL_PHASES === 0;
        if (!shouldChangeAsset) {
            return;
        }

        this.framesSpentCelebrating = 0;
        if (this.assetName === Constants.RHINO_EAT_6) {
            this.assetName = Constants.RHINO_EAT_5;
        } else {
            this.assetName = Constants.RHINO_EAT_6;
        }
    }
}