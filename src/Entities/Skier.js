import * as Constants from "../Constants";
import { StatefulEntity } from "./StatefulEntity";
import { clamp } from "../Core/Utils";

export class Skier extends StatefulEntity {
    static STARTING_SPEED = 10;
    static DIAGONAL_SPEED_REDUCER = 1.4142;
    static FRAMES_PER_JUMP_STATE = 10;

    assetName = Constants.SKIER_DOWN;
    state = Constants.SKIER_STATES.SKIING;

    speed = Skier.STARTING_SPEED;

    direction = Constants.SKIER_TRAJECTORIES.DOWN;

    framesInAir = 0;
    jumpCooldownTimer = 0;

    constructor(x, y) {
        super(x, y);
    }

    setDirection(direction) {
        this.direction = direction;
        this.updateAsset(); 
    }

    updateAsset() {
        if (this.isDead()) {
            this.assetName = undefined;
            return;
        }

        if (this.isJumping()) {
            this.assetName = Constants.SKIER_JUMPING_ASSET[this.state];
        } else {
            this.assetName = Constants.SKIER_DIRECTION_ASSET[this.direction];
        }
    }

    move() {
        this.jumpCooldownTimer = clamp(this.jumpCooldownTimer - 1, 0);

        switch (this.state) {
            case Constants.SKIER_STATES.DEAD:
                return;
            case Constants.SKIER_STATES.JUMP_START:
            case Constants.SKIER_STATES.JUMP_TUCK:
            case Constants.SKIER_STATES.JUMP_ROLL:
            case Constants.SKIER_STATES.JUMP_LAND:
                return this.stickTheLanding();
        }

        switch(this.direction) {
            case Constants.SKIER_TRAJECTORIES.LEFT:
                return this.moveLeft();
            case Constants.SKIER_TRAJECTORIES.LEFT_DOWN:
                return this.moveLeftDown();
            case Constants.SKIER_TRAJECTORIES.DOWN:
                return this.moveDown();
            case Constants.SKIER_TRAJECTORIES.RIGHT_DOWN:
                return this.moveRightDown();
            case Constants.SKIER_TRAJECTORIES.RIGHT:
                return this.moveRight();
        }
    }

    moveLeft() {
        this.x -= this.speed;
    }

    moveLeftDown() {
        this.x -= this.speed / Skier.DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Skier.DIAGONAL_SPEED_REDUCER;
    }

    moveDown() {
        this.y += this.speed;
    }

    moveRightDown() {
        this.x += this.speed / Skier.DIAGONAL_SPEED_REDUCER;
        this.y += this.speed / Skier.DIAGONAL_SPEED_REDUCER;
    }

    moveRight() {
        this.x += this.speed;
    }

    moveUp() {
        this.y -= this.speed;
    }

    turnLeft() {
        if (this.isDead()) {
            return;
        } else if (this.isJumping()) {
            return;
        }

        this.setState(Constants.SKIER_STATES.SKIING);
        if (this.direction === Constants.SKIER_TRAJECTORIES.LEFT) {
            this.moveLeft();
        } else if (this.direction === Constants.SKIER_TRAJECTORIES.STATIONARY) {
            this.setDirection(Constants.SKIER_TRAJECTORIES.LEFT);
        } else {
            const newDirection = clamp(this.direction - 1, Constants.SKIER_TRAJECTORIES.LEFT, Constants.SKIER_TRAJECTORIES.RIGHT);
            this.setDirection(newDirection);
        }
    }

    turnRight() {
        if (this.isDead()) {
            return;
        } else if (this.isJumping()) {
            return;
        }

        this.setState(Constants.SKIER_STATES.SKIING);
        if (this.direction === Constants.SKIER_TRAJECTORIES.RIGHT) {
            this.moveRight();
        } else if (this.direction === Constants.SKIER_TRAJECTORIES.STATIONARY) {
            this.setDirection(Constants.SKIER_TRAJECTORIES.RIGHT);
        } else {
            const newDirection = clamp(this.direction + 1, Constants.SKIER_TRAJECTORIES.LEFT, Constants.SKIER_TRAJECTORIES.RIGHT);
            this.setDirection(newDirection);
        }
    }

    turnUp() {
        if(this.direction === Constants.SKIER_TRAJECTORIES.LEFT || this.direction === Constants.SKIER_TRAJECTORIES.RIGHT) {
            this.moveUp();
        }
    }

    turnDown() {
        if (this.state === Constants.SKIER_STATES.SKIING) {
            this.setDirection(Constants.SKIER_TRAJECTORIES.DOWN);
        }
    }

    canJump() {
        if (this.direction === Constants.SKIER_TRAJECTORIES.STATIONARY) {
            return false;
        } else if (this.state >= Constants.SKIER_STATES.JUMP_START) {
            return false;
        }

        return !this.jumpCooldownTimer;
    }

    isJumping() {
        return this.state >= Constants.SKIER_STATES.JUMP_START;
    }

    jump() {
        if (this.canJump()) {
            this.setState(Constants.SKIER_STATES.JUMP_START);
        }
    }

    stickTheLanding() {
        this.moveDown();

        const shouldChangeState = ++this.framesInAir % Skier.FRAMES_PER_JUMP_STATE === 0;
        if (shouldChangeState) {
            if (this.state === Constants.SKIER_STATES.JUMP_LAND) {
                this.framesInAir = 0;
                this.jumpCooldownTimer = Skier.FRAMES_PER_JUMP_STATE * 5;
                this.setState(Constants.SKIER_STATES.SKIING);
                this.turnDown();
            } else {
                this.setState(this.state + 1);
            }
        }
    }

    crash() {
        this.setDirection(Constants.SKIER_TRAJECTORIES.STATIONARY);
        this.setState(Constants.SKIER_STATES.CRASHED);
    }

    die() {
        this.setDirection(Constants.SKIER_TRAJECTORIES.STATIONARY);
        this.setState(Constants.SKIER_STATES.DEAD);
    }

    isDead() {
        return this.state === Constants.SKIER_STATES.DEAD;
    }
}